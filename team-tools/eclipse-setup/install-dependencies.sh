#!/bin/bash

FILE_NAME="eclipse.tar.gz"

echo "installing packages for umbrella eclipse setup"
echo "This installer assumes you are using apt package manager, pointed at"
echo "the normal ubuntu repos."

# Update and install prerequisite packages
apt update
apt install openjdk-11-jdk \
	    srecord \
	    openocd \
	    arm-none-eabi-gcc

# Get eclipse installer

echo "download eclipse..."
wget --tries=3 -O $FILE_NAME https://gemmei.ftp.acc.umu.se/mirror/eclipse.org/oomph/epp/2022-03/R/eclipse-inst-jre-linux64.tar.gz

tar -xzf $FILE_NAME

echo "deleteing archive"
rm $FILE_NAME

echo "last bit of eclipse installation is done manually"

cd ./eclipse-installer

./eclipse-inst > /dev/null 2>&1
