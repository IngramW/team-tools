#!/bin/sh

# Requires installation of openconnect.
# e.g. apt install openconnect


echo "Attempting connection to BRIL VPN"
echo "You will be prompted for a password, use your pin + token"
echo ""


if [$1 == ""]; then
  echo "Please enter username as arg -> firstname.lastname"
  echo "Connect process aborted"
  echo ""
  exit 1
fi

USER=$1

openconnect --protocol=anyconnect \
	    --user=$USER \
	    --server=vpn.toshiba-trel.com:4433 \
	    --verbose \
	    --syslog
